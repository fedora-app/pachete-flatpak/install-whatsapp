#!/bin/bash

# Verifică dacă utilizatorul este root
if [[ $EUID -ne 0 ]]; then
   echo "Acest script trebuie să fie rulat ca root" 
   exit 1
fi

# Instalează Flatpak și Flatpak plugin pentru DNF
dnf install flatpak flatpak-plugin-flatpak -y

# Adaugă repo-ul Flathub
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Instalează WhatsApp for Linux folosind Flatpak
flatpak install flathub com.github.eneshecan.WhatsAppForLinux -y

echo "WhatsApp for Linux a fost instalat cu succes!"

