# Instalator WhatsApp for Linux folosind Flatpak

## Descriere
Acest script bash simplu automatizează procesul de instalare a WhatsApp for Linux folosind Flatpak. Flatpak este un sistem de distribuție de aplicații pentru Linux care permite instalarea și rularea aplicațiilor în containere izolate.

## Cerințe de sistem
- Linux
- Acces de administrator (root)

## Utilizare
1. Asigurați-vă că rulați scriptul ca root.
2. Rulați scriptul bash folosind comanda:
    ```bash
    ./install-whatsapp.sh
    ```

## Procedură
- Verifică dacă utilizatorul este root și iese din script dacă nu este.
- Instalează Flatpak și pluginul Flatpak pentru DNF.
- Adaugă repo-ul Flathub, care conține aplicații Flatpak populare.
- Instalează WhatsApp for Linux folosind Flatpak din Flathub.
- Afisează un mesaj de confirmare după instalare.

## Notă
Acest script este creat doar pentru scopuri de demonstrație și poate necesita ajustări pentru a se potrivi cu mediul dvs. specific.